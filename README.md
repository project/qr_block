# QR Block

## Content of this file

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


## Introduction

This module enables the creation of a dynamic QR Block Image.
Place block anywhere and add token value to get dynamic value.


## Requirements

This module requires the following modules:

- [Token](https://www.drupal.org/project/token)
- [Block](https://drupal.org/project/block)


## Installation

Install as you would normally install a contributed Drupal module. See:
[Installing Drupal Modules](https://drupal.org/documentation/install/modules-themes/modules-8) for further
information.


## Configuration

- Visit the block configuration page at Administration » Structure » Block Layout. 
  Click on the Configure link for a block.


## Maintainers

- Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
- Radheshyam Kumawat (radheymkumar) - https://www.drupal.org/u/radheymkumar
